//
//  DetailViewController.h
//  KMKCellExample
//
//  Created by Eugene Klishevich on 30/10/15.
//  Copyright © 2015 Eugene Klishevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface DetailViewController : UIViewController

@property (strong, nonatomic) id detailItem;
@property (weak, nonatomic) IBOutlet UILabel *detailDescriptionLabel;

@end

