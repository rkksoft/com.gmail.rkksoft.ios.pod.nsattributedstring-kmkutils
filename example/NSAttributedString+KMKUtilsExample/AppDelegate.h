//
//  AppDelegate.h
//  KMKCellExample
//
//  Created by Eugene Klishevich on 30/10/15.
//  Copyright © 2015 Eugene Klishevich. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

